var cc       = require('config-multipaas');

var config_overrides = {}; // { PORT: 80, IP: "0.0.0.0" } // override values should be provided first 

var config   = cc(config_overrides);

// Load the http module to create an http server.
var http = require('http');

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(function (request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end("Hello From OpenShift! The time is: " + Date().toString() + "\n");
  console.log("Request @ " + Date().toString());
});

// Listen on port 8000, IP defaults to 127.0.0.1
// server.listen(80);

server.listen(config.get('PORT'), config.get('IP'), function () {
  console.log("Server running now");
  console.log( "Listening on " + config.get('IP') + ", port " + config.get('PORT') )
});

// Put a friendly message on the terminal